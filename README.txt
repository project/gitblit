## Installation:

1. Install last JDK (Java Development Kit)
2. Download and unzip Gitblit GO on http://gitblit.com
3. Open data/gitblit.properties in your favorite text editor and make sure to 
   review and set : server.httpPort and server.httpsPort
4. Write a small bash script /etc/init.d/gitblit :

   #!/bin/bash
   case $1 in
        start)
            echo "Starting gitblit"
            cd /path_to_gitblit_folder
            java -jar gitblit.jar --baseFolder data >/dev/null 2>&1&
            ;;
        stop)
            echo "Stop gitblit"
            cd /path_to_gitblit_folder
            java -jar gitblit.jar --baseFolder data --stop >/dev/null 2>&1&
            ;;
        restart)
            $0 stop
            sleep 3
            $0 start
            ;;
    esac
    exit 0

5. Download and install the gitblit module from drupal.org and enable it
6. Visit admin/config/development/gitblit