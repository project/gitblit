<?php

/**
 * @file
 * Contains the main Gitblit functionality and repositories admin pages.
 */

/**
 * List gitblit repositories.
 */
function gitblit_repositories_list() {
  drupal_set_title(t('Gitblit repositories'));
  $repositories = _gitblit_request(array('req' => 'LIST_REPOSITORIES'));
  $table = array(
    '#header' => array(
      array('data' => t('Repository')),
      array('data' => t('Description')),
      array('data' => t('Size')),
      array('data' => t('Changed')),
      t('Operations'),
    ),
    '#theme' => 'table',
    '#attributes' => array(),
    'id' => array(),
    '#colgroups' => array(),
    '#sticky' => FALSE,
    '#empty' => t('No results found.'),
    '#rows' => array(),
  );
  if (!empty($repositories)) {
    foreach ($repositories as $repo_url => $repo) {
      $parse_url = parse_url($repo_url);
      if (variable_get('gitblit_server_rewrite_url', 1)) {
        $view_url = $parse_url['scheme'] . "://" . $parse_url['host'] . ":" . 
                variable_get('gitblit_server_port', 8080) . "/summary/" . urlencode($repo['name']);
        $edit_url = $parse_url['scheme'] . "://" . $parse_url['host'] . ":" . 
                variable_get('gitblit_server_port', 8080) . "/edit/" . urlencode($repo['name']);
      }
      else {
        $view_url = $parse_url['scheme'] . "://" . $parse_url['host'] . ":" . 
                variable_get('gitblit_server_port', 8080) . "/summary/?r=" . $repo['name'];
        $edit_url = $parse_url['scheme'] . "://" . $parse_url['host'] . ":" . 
                variable_get('gitblit_server_port', 8080) . "/edit/?r=" . $repo['name'];
      }
      $table['#rows'][] = array(
        isset($repo['displayName']) ? $repo['displayName'] : $repo['name'],
        $repo['description'],
        $repo['size'],
        $repo['lastChange'],
        theme(
          'links',
          array(
            'links' => array(
              array('title' => t('view'), 'href' => $view_url),
              array('title' => t('edit'), 'href' => $edit_url),
            ),
          )
        ),
      );
    }
  }
  else {
    drupal_set_message(t('Gitblit server Error'), 'error');
  }
  $output = array(
    'table' => $table,
    'pager' => array(
      '#theme' => 'pager',
      array('tags' => array()),
    ),
  );

  return $output;
}
