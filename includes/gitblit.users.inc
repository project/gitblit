<?php

/**
 * @file
 * Contains the main Gitblit functionality and users admin pages.
 */

/**
 * List all gitblit users.
 */
function gitblit_users_list() {
  drupal_set_title(t('Gitblit users'));
  $users = _gitblit_request(array('req' => 'LIST_USERS'));
  $table = array(
    '#header' => array(
      array('data' => t('Username')),
      array('data' => t('Names')),
      array('data' => t('mail')),
      array('data' => t('Type')),
      array('data' => t('repositories')),
      array('data' => t('Disabled')),
      isset($drupal_account->uid) ? t('Operations') : NULL,
    ),
    '#theme' => 'table',
    '#attributes' => array(),
    'id' => array(),
    '#colgroups' => array(),
    '#sticky' => FALSE,
    '#empty' => t('No results found.'),
    '#rows' => array(),
  );
  if (!empty($users)) {
    foreach ($users as $gitblit_uid => $gitblit_account) {
      $drupal_account = NULL;
      $table['#rows'][] = array(
        $gitblit_account['username'],
        isset($gitblit_account['displayName']) ? $gitblit_account['displayName'] : '',
        $gitblit_account['emailAddress'],
        $gitblit_account['accountType'],
        count($gitblit_account['repositories']),
        $gitblit_account['disabled'] ? t('Yes') : t('No'),
        theme(
          'links',
          array(
            'links' => array(
              isset($drupal_account->uid) ? array('title' => t('edit'), 'href' => "user/$drupal_account/edit") : NULL,
            ),
          )
        ),
      );
    }
  }
  else {
    drupal_set_message(t('Gitblit server Error'), 'error');
  }
  $output = array(
    'table' => $table,
    'pager' => array(
      '#theme' => 'pager',
      array('tags' => array()),
    ),
  );

  return $output;
}

function _gitblit_account($username, $user_model) {
  $query = array('req' => 'GET_USER', 'name' => $username);
  $gitblit_account = _gitblit_request($query);
  if (empty($gitblit_account)) {
    // Account doesn't exists, let create it.
    $query = array('req' => 'CREATE_USER', 'name' => $username);
    $gitblit_account = _gitblit_request($query, $user_model, 'POST');
  }
  else {
    // Account exists, update it.
    $query = array('req' => 'EDIT_USER', 'name' => $username);
    $gitblit_account = _gitblit_request($query, $user_model, 'POST');
  }
  krumo($gitblit_account);die;
}

function _gitblit_create_account() {
  
}

function _gitblit_edit_account() {
  
}

function _gitblit_delete_account() {
  
}
