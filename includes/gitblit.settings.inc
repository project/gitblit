<?php

/**
 * @file
 * Contains the main Gitblit functionality.
 */

/**
 * Menu callback for gitblit admin settings.
 */
function gitblit_admin_settings_form() {
  drupal_set_title(t('Gitblit settings'));
  $form = array();
  $form['gitblit_server_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('gitblit_server_username', ''),
    '#required' => TRUE,
  );
  $form['gitblit_server_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => variable_get('gitblit_server_password', ''),
  );
  $form['gitblit_server_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Gitblit host'),
    '#default_value' => variable_get('gitblit_server_host', 'http://localhost'),
    '#description' => t('Set this to the server IP or hostname that your Gitblit server runs on (e.g. http://127.0.0.1).'),
    '#maxlength' => 512,
    '#required' => TRUE,
  );
  $form['gitblit_server_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => variable_get('gitblit_server_port', 8080),
    '#description' => t('Set this to the server IP or hostname that your Gitblit server runs on (e.g. 8080).'),
    '#maxlength' => 4,
    '#required' => TRUE,
  );
  $form['gitblit_server_rewrite_url'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable clean URLs for Gitblit'),
    '#default_value' => variable_get('gitblit_server_rewrite_url', 1),
    '#description' => t('Use URLs like example.com/user instead of example.com/?q=user.')
  );
  return system_settings_form($form);
}

/**
 * Validate the Gitblit settings.
 */
function gitblit_admin_settings_form_validate($form, &$form_state) {
  $gitblit_server_host = $form_state['values']['gitblit_server_host'];
  // Check if it's a valid url.
  if (!valid_url($gitblit_server_host, TRUE)) {
    form_set_error('gitblit_server_host', t('This is not a valid URL: !url.', array('!url' => $gitblit_server_host)));
  }
}
